﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace funWithStrings.Test
{
    [TestFixture]
    public class StringWarmsTest
    {
        [TestCase("Bob","Hello Bob!")]
        public void Exercise1Test(string input, string expected)
        {
               //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.SayHi(input);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        public void Exercise1Test(string input1, string input2, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.Abba(input1,input2);

            //Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
